class apt_listchanges::present {

  package { 'apt-listchanges':
    ensure => present;
  }

  file { '/etc/apt/listchanges.conf':
    content => epp('apt_listchanges/listchanges.epp', {
      'ensure'   => $apt_listchanges::ensure,
      'frontend' => $apt_listchanges::frontend,
      'email'    => $apt_listchanges::email,
      'confirm'  => $apt_listchanges::confirm,
      'saveseen' => $apt_listchanges::saveseen,
      'which'    => $apt_listchanges::which,
    }),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package['apt-listchanges'];
  }
}
