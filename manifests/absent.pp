class apt_listchanges::absent {

  package { 'apt-listchanges':
    ensure => purged;
  }

  file { [ '/etc/apt/listchanges.conf', '/etc/apt/apt.conf.d/20listchanges' ]:
    ensure => absent;
  }
}
