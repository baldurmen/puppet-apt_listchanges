class apt_listchanges (
  Enum[present, absent] $ensure = present,
  Enum[ 'pager', 'browser', 'xterm-pager', 'xterm-browser',
        'text', 'mail', 'gtk', 'none'] $frontend = 'mail',
  String $email = 'root',
  Boolean $confirm = false,
  String $saveseen = '/var/lib/apt/listchanges.db',
  Enum['news', 'changelog', 'both'] $which = 'both',
){

  if $apt_listchanges::ensure == 'present' {
    include ::apt_listchanges::present
  }
  else {
    include ::apt_listchanges::absent
  }
}
