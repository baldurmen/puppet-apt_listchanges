# apt_listchanges

## Description

This is a simple module to install and configure `apt-listchanges` for you.

## Usage

The simplest way to use this module is this way:

    include apt_listchanges

A more advanced example might look like:

    class {'apt_listchanges':
      ensure   => present,
      confirm  => false,
      frontend => 'mail',
      email    => 'foo@bar.com',
      saveseen => `/var/lib/apt/listchanges.db`,
      which    => 'both';
    }

## Parameters

The following parameters are available in the `apt_listchanges` class:

### `ensure`

Optional.

Data type: Enum.

This parameter can take two different values:

 * `present`
 * `absent`

If set to `present`, this module will install `apt-listchanges` and configure
it. If set from `present` to `absent`, `apt-listchanges` and its configuration
will be removed.

Default value: `present`.

### `frontend`

Optional.

Data type: Enum.

The type of frontend you want `apt-listchanges` to use. This parameter can take
one of these values:

 * `pager`
 * `browser`
 * `xterm-pager`
 * `xterm-browser`
 * `text`
 * `mail`
 * `gtk`
 * `none`

Default value: `mail`.

### `email`

Optional.

Data type: String.

If you use the `mail` frontend, this parameters tells `apt-listchanges` what
email to use.

Default value: `root`.

### `confirm`

Optional.

Data type: Boolean.

Once changelogs have been displayed, ask the user whether or not to proceed.

Default value: false.

### `saveseen`

Optional.

Data type: String.

Path to the file where the apt-listchanges database of seen packages is stored.

Default value: `/var/lib/apt/listchanges.db`.

### `which`

Optional.

Data type: Enum.

This parameter takes three different values:

 * `news`
 * `changelogs`
 * `both`

This option selects whether news, changelogs  or both should be displayed.

Default valuue: `both`.

## Limitations

This module was developed for Debian Stretch and Puppet 4. Compatibility on
other OS or Puppet versions is not guaranteed.
